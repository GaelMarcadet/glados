

# Git Workflow 
## Git commit naming conventions

To have a clear git repository, I encourage you to use this simple commit name format:
**\<type>: \<message>** where type indicates kind of commited modifications.

Available types are:

| Type | Description |
| ---- | ---- |
| Feature | Commit to add a feature |
| Bugfix | Commit to fix a bug |
| Style | Improve application style |
| Refactor | Refactoring, cleaning code |
| Infra | Technical commit |

For instance, to indicate refactoring commit, i can indicate in commit message: *refactor: Stegano.py refactored*.

## Git tips

To avoid any problem during your contributing, please do your work in an appropriate branch. Keep in mind to fetch regulary your work with source branch ;)




# Python Source code

## Naming Conventions

To improve code writting and reading, all code writted in *Python* must have to respect some rules and conventions.

In this project, all functions and variables must be writted with only **lowercases**, and spaces are represented with an **_** (underscore). So, a function name doWorkProperly should be written as:

```python
def do_work_properly():
    pass
```

This naming convention is also available for variables, exception for constants that are represents with **uppercases** like **MAXIMUM_PIXEL**.

## Recommandations

This section includes all recommandations to write a user-friendly code.
It's not a mandatory to follow these few simples tips, but this can make the differences.

### Use spaces and  more  and more    spaces  and        mo r  e m o  re 

Don't compress your code, your loops, etc...
Use white space and returns to exhibe each code sections.

```python
MAX = 10
OFFSET = 5
def my_function( param : type ):
    # a section
    my_var = call_to_function( 1, 2, 3 )

    # another section
    for i in range( 1, MAX, OFFSET ):
        j = i + MAX
```

### Description of file

At the beginning of each python file, a short description should describe himself.
Filename, author, date of creation, last update and file description must be exposed.
You can look an example below:

```py
###############################################################################
# File: stegano.py
# Created: 08/10/19
# Updated: 08/10/19
# Authors: Gael Marcadet
# Description: Reveal message from image by using reverse steganographic 
# process
###############################################################################
```

When you modify a file that's not your own, indicates your name after in authors and updates information.
