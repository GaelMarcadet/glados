###############################################################################
# File: stegano.py
# Created: 08/10/19
# Updated: 08/10/19
# Authors: Gael Marcadet
# Description: Reveal message from image by using reverse steganographic 
# process
###############################################################################

import matplotlib.image as mpimg
import numpy as np
import matplotlib.pyplot as plt


def open_image( filename ):
    return mpimg.imread( filename )



PIXEL_LIMIT = 255

def reveal_stegano( filename, affect_red : bool = True, affect_green : bool = True, affect_blue : bool = True  ):
    """ Creates a new image that's contains information hidden in image filename """

    def extract_from_pixel( pixel ):
        """ Returns all information extracted from piexel """
        # extract colors information
        r, g, b = pixel

        # extract pieces of information from specified pixels
        affectation_orders = [ 
            ( r, affect_red ), 
            ( g, affect_green ), 
            ( b, affect_blue ),
        ]
        r, g, b = [ ( value % 2 ) * PIXEL_LIMIT if affect else 0 for value, affect in affectation_orders ]

        return ( r, g, b )

    # open and copy image to keep a trace of original image
    img = open_image( filename )
    copy = np.copy( img )

    # extracts all information of each pixel and put them in copy image
    width_size, height_size, components = img.shape
    for height in range( height_size ):
        for width in range( width_size ):
            copy[ width, height ] = extract_from_pixel( img[ width, height ] ) 

    return copy

def show( image ):
    """ Expose an image """
    plt.imshow( image )
    plt.show()
