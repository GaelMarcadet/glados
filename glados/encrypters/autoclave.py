###############################################################################
# File: autoclave.py
# Created: 11/10/19
# Updated: 11/10/19
# Authors: Gael Marcadet
# Description: Encrypts, decrypts and breaks autoclave encryption.
###############################################################################
from glados.utils import letter_to_index, index_to_letter, to_string, build_key, build_key_decrypt, text_weight, clean
from glados.encrypters.vigenere import encrypt_Vigenere,  decrypt_vigenere
from typing import List




def autoclave_encrypt( k, m ):
    k = k + m
    return encrypt_Vigenere( m, k )

def autoclave_decrypt( k, c ):
    k = k[ :len( c ) ]
    key_length = len( k )   
    current_key = k
    message = ""
    
    while len( c ) > 0:
        partial_message = decrypt_vigenere( c[ :key_length ], current_key )
        current_key = partial_message
        message = message + partial_message
        c = c[ key_length: ]
    
    return message

def autoclave_information( c, start_index, offset = 1 ):
    keys = []
    
    for key_index in range( 26 ):
        
        key = build_key( [key_index] )
        message = key
        for letter_index in range( start_index, len( c ), offset ):
            m = decrypt_vigenere( c[ letter_index ], key )
            key = m
            message += m
     
        #i = ic( do_freq( message ) )
        i = text_weight( message )
        
        
        keys.append( ( i, build_key_decrypt(key), message ) )
    keys.sort( reverse = True )
    return keys

def autoclave_break( c, min_key_length = 3, max_key_length = 12 ):
    
    def extract_message( results ):
        founded_messages = [ m for _, _, m in results ]

        max_length = max( [ len(m) for m in founded_messages ] )

        final_message = []
        for index in range( max_length ):
            final_message += [ m[ index ] for m in founded_messages if index < len( m ) ]

        return ''.join( final_message )
    
    def extract_key( ciphered, clear, key_length ):
        return decrypt_vigenere( ciphered[ :key_length ], clear[ :key_length ] )
    
    # starting defining key length
    results = []
    for key_length in range( min_key_length, max_key_length ):
        key_results = []

        for offset in range( key_length ):
            key_results.append( autoclave_information( c, 
                                                     start_index = key_length + offset, 
                                                     offset = key_length )[0] )
            
        key_message = extract_message( key_results )
        key = extract_key( c, key_message, key_length )
        key_score = text_weight( key_message )
        
        results.append( ( key_score, key, key_message ) )
    
    results.sort( reverse = True )
    return results[ 0 ]


if __name__ == "__main__":
    autoclave_c= clean("""
    VOMDHTZILNIMMIYQGYPRQZSAXWGKHRFXLBKVWBWIIKMKVWZSZWLVOMHKGHVXVERSTXUQXAVBATNCIMFG
    UGIJVCVRUGEIQQADEWKATULPFQWSNTIYEWSRBSTTUJUFHKSLOZOCKUDPLHYHXQJIDGXZAVBEJAKSAUFX
    XTKFLXLUSJIFFSUVLARIFTMEKJZVPQDWKYWMQHUTYQHENYHHXXVNWAZKGTLUNZFLCOKISQUDSFPDDLEF
    FBLRUGOIGUHURXSQRLSUZDISUZEKXGHRFHXNUJDVHWPTAVDLVAZHICPSEQGPIXVJXMHRBTRBVLIZHVGV
    STSAVTUSJMEKKYXEWCLORBPLJYTAFCOHYMYYNPVOEDRHRDBBLYHNUPLRGFSRRPXHFTUIVSHIRFXZRRZX
    CVVUTDITCXVXQENVKSAXBYIYTNYRCECKUYUBULWRNTIKWUKWWDAHHMMOGNMZBFUHBRGAIVMFXHCNQTLO
    RDKXANEKYTJJUFEQSYLYLJIRRRQXFJPYMOBFVXRAPVEJMYKOFBVSSFHQYKDOLUMZTECXBXENYVRNHMIY
    VHMEESBUKLDMYKSNRPHFFEXKJDVHWPTAVDLVAZHICPSEQGPGZICIGRAUHLHAIHXIIOLURSBKXEONHGKS
    XXRRWFLIBMZSGEIKOSMPDWHODTURRFSQPGRXSSWPIMVYXPWPYNLYDONPSCDTELWTZACKKWHJVKJHTJHF
    NVGHNLYEYGFESATRMAUGBIILOKWSHLXDAQLSFACVRGGIFWXCWCTEASCTBXPLTTVTGGHRMAZDLVRLZGZI
    FUGBSQQAASQWHVSXWFHHRSLXBAQROMLYEQPSPMSYOWGAHWOCSRXZVMJTRKAFSENFMVVLRBZVVWMSIGZX
    OSGKHBFSQHUIDRHWPTAVDLVAZHICPSEQGPAYLWILVHWIXMMVQEXQLXSEQGPIXLJMVSVQQRTUGIRQKQSG
    LTXSEQGPIQMZPNFRXLXTVVAFWQOSGLTXSEQGPWSQGIUVCCIBMCEKLJTOKAETMPSEQGPAYLRLEABKBVRB
    SUUMZMLOJAAETMPSEQGPSEQGPSEQGPSEQGPSEQGP""")
    
    print( autoclave_break( autoclave_c ) )